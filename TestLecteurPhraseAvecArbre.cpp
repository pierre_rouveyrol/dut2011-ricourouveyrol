#include <iostream>
#include <fstream>
using namespace std;
#include "LecteurPhraseAvecArbre.h"

int main(int argc, char* argv[]) {
	char nomFich[255];
	if (argc != 2) {
		cout << "Usage : " << argv[0] << " nom_fichier_source" << endl << endl;
		cout << "Entrez le nom du fichier que voulez-vous interpreter : ";
		cin.getline(nomFich, sizeof(nomFich));
	} else
		strncpy(nomFich, argv[1], sizeof(nomFich));

	LecteurPhraseAvecArbre lp(nomFich);
	lp.analyse();
	cout << endl << "Arbre Abstrait : " << endl;
	cout         << "================"  << endl;
	lp.getArbre()->afficher();
	cout << endl << "Table des symboles avant evaluation : " << lp.getTs();
	cout << endl << "Evaluation de l'arbre (interpretation)..." << endl;
	lp.getArbre()->getValeur();
	cout << endl << "Table des symboles apres evaluation : " << lp.getTs();
        //--------------------------------------
        fstream fichierC;
        string monfichier="transC.cpp";
        string code= "";
        fichierC.open(monfichier.c_str(), ios_base::out );
        code = code + "#include <iostream>\n#include <string>\n#include <stdlib.h>\n\n";
        code = code + "using namespace std;\n\n";
        code = code + "int main() {\n";
        code = code + lp.getTs().transcode();
        code = code.data() + lp.getArbre()->transCodage();
        code = code + "return EXIT_SUCCESS;";
        code = code + "\n}";
        cout << code << endl;
        //---------------------------------------
        size_t longueur = code.size();
        fichierC.write(code.data(), longueur);
        fichierC.close();
        //---------------------------------------
        
	return 0;
}
