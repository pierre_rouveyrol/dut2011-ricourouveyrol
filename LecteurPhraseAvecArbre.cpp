#include "LecteurPhraseAvecArbre.h"

#include <stdlib.h>
#include <iostream>
using namespace std;

#include "Exceptions.h"

////////////////////////////////////////////////////////////////////////////////
LecteurPhraseAvecArbre::LecteurPhraseAvecArbre(string nomFich) :ls(nomFich), ts() {}

////////////////////////////////////////////////////////////////////////////////
void LecteurPhraseAvecArbre::analyse() {
// <analyse> ::= <programme>
	arbre = programme();
	cout << "Syntaxe correcte." << endl;
}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::programme() {
// <programme> ::= debut <seqInst> fin FIN_FICHIER

	sauterSymCour("debut");
        Noeud* si = seqInst();
	sauterSymCour("fin");
	testerSymCour("<FINDEFICHIER>");

        return si;
}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::seqInst() {
// <seqInst> ::= <inst> ; { <inst> ; }

        NoeudSeqInst* si = new NoeudSeqInst();
	do {
		si->ajouteInstruction(inst());
		sauterSymCour(";");
	} while (ls.getSymCour()=="<VARIABLE>" || ls.getSymCour()=="si" || ls.getSymCour()=="tantque" || ls.getSymCour()=="repeter" || ls.getSymCour()=="pour" || ls.getSymCour()=="switch");
	// tant que le symbole courant est un debut possible d'instruction...
        return si;
}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::inst() {
// <inst> ::= <affectation> | <instSi> | <instTq> | <instRepeter> | <instSwitch>
        if (ls.getSymCour() == "si")  return instSi();
        else if (ls.getSymCour() == "tantque") return instTq();
        else if (ls.getSymCour() == "repeter") return instRepeter();
        else if (ls.getSymCour() == "ecrire" ) return instEcrire();
        else if (ls.getSymCour() == "lire" ) return instLire();
        else if (ls.getSymCour() == "pour" ) return instPour();
        else if (ls.getSymCour() == "switch" ) return instSwitch();
        else return affectation();

}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::affectation() {
// <affectation> ::= <variable> = <expression>
        Noeud* exp;
        testerSymCour("<VARIABLE>");
        Noeud* var = ts.chercheAjoute(ls.getSymCour());
        ls.suivant();
        if (ls.getSymCour()=="="){
            sauterSymCour("=");
            if (ls.getSymCour()=="<CHAINE>" || ls.getSymCour()=="<CARACTERE>") {
                exp = ts.chercheAjoute(ls.getSymCour());
                ls.suivant();
            }
            else exp = expression();
        }
        else if (ls.getSymCour()=="+"){
            sauterSymCour("+");
            sauterSymCour("+");
            exp = new NoeudOperateurBinaire(Symbole("+"),var,ts.chercheAjoute(Symbole("1")));
        }  
        
        return new NoeudAffectation(var,exp);
}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::expression() {
// <expression> ::= <terme> { <opAdd> <terme> }

	Noeud* fact = terme();
	while (ls.getSymCour()=="+" || ls.getSymCour()=="-" ) {
		Symbole operateur = opAdd();
		Noeud* factDroit = terme();
                fact = new NoeudOperateurBinaire(operateur,fact,factDroit); // const. du noeud
	}
        return fact;
}

////////////////////////////////////////////////////////////////////////////////
Noeud* LecteurPhraseAvecArbre::facteur() {
// <facteur> ::= <entier>  |  <variable>  |  <opUnaire> (<facteur>)  |  ( <expBool> )


        Noeud* fact=NULL;
	if (ls.getSymCour()=="<VARIABLE>" || ls.getSymCour()=="<ENTIER>" || ls.getSymCour()=="<REEL>" || ls.getSymCour()=="<CHAINE>" || ls.getSymCour()=="<CARACTERE>") {
                fact = ts.chercheAjoute(ls.getSymCour());
		ls.suivant();
    } else if (ls.getSymCour()=="(") {
		ls.suivant();
		fact = expBool();
		sauterSymCour(")");
	} else if ( ls.getSymCour() == "-") {
                ls.suivant();
                sauterSymCour("(");
                fact = new NoeudOperateurBinaire(opUnaire(),ts.chercheAjoute(Symbole("0")),facteur());
                sauterSymCour(")");
        } else if (ls.getSymCour() == "non") {
                ls.suivant();
                sauterSymCour("(");
                fact = new NoeudOperateurBinaire(Symbole("^"),ts.chercheAjoute(Symbole("1")),expBool());
                sauterSymCour(")");
        } else {
		erreur("<facteur>");
        }

        return fact;
}

////////////////////////////////////////////////////////////////////////////////
void LecteurPhraseAvecArbre::testerSymCour(string ch) {
	/*if (ls.getSymCour() != ch) {
		cout << endl << "-------- Erreur ligne " << ls.getLigne()
				<< " - Colonne " << ls.getColonne() << endl << "   Attendu : "
				<< ch << endl << "   Trouve  : " << ls.getSymCour() << endl
				<< endl;
		exit(0);
	}*/
        try{
           	if (ls.getSymCour() != ch) {
                       throw ErreurType("Erreur de type", ls.getLigne(), ls.getColonne(), ch, ls.getSymCour().getChaine());
                }
        }
        catch(ErreurType const& e){
                cerr << e.what() << "Ligne " << e.getLigne() << " - Colonne " << e.getColonne() << endl << "   Attendu : " << e.getAttendu() << endl << "   Trouvé : " << e.getTrouve() << endl;
                exit(1);
        }
}

////////////////////////////////////////////////////////////////////////////////
void LecteurPhraseAvecArbre::sauterSymCour(string ch) {
	testerSymCour(ch);
	ls.suivant();
}

////////////////////////////////////////////////////////////////////////////////
void LecteurPhraseAvecArbre::erreur(string mess) {
	/*cout << endl << "-------- Erreur ligne " << ls.getLigne() << " - Colonne "
			<< ls.getColonne() << endl << "   Attendu : " << mess << endl
			<< "   Trouve  : " << ls.getSymCour() << endl << endl;
	exit(0);*/
        try{
                  throw ErreurType("Erreur de type", ls.getLigne(), ls.getColonne(), mess, ls.getSymCour().getChaine());
           }
                catch(ErreurType const& e){
                cerr << e.what() << "Ligne " << e.getLigne() << " - Colonne " << e.getColonne() << endl << "   Attendu : " << e.getAttendu() << endl << "   Trouvé : " << e.getTrouve() << endl;
                exit(1);
        }
}

Noeud* LecteurPhraseAvecArbre::terme() {
// <terme> ::= <facteur> { <opMult> <facteur> }
    Noeud* fact = facteur();
    while ( ls.getSymCour()=="*" || ls.getSymCour()=="/") {
        Symbole operateur = opMult();
        Noeud* factDroit = facteur();
        fact = new NoeudOperateurBinaire(operateur,fact,factDroit);
    }
    return fact;
}

Symbole LecteurPhraseAvecArbre::opAdd() {
//opAdd ::= - | +
    Symbole operateur;
    if ( ls.getSymCour() == "-" | ls.getSymCour() == "+") {
        operateur = ls.getSymCour();
        ls.suivant();
    }
    else {
        erreur("<opAdd>");
    }
    return operateur;
}

Symbole LecteurPhraseAvecArbre::opMult() {
//opMult ::= * | /
    Symbole operateur;
    if ( ls.getSymCour() == "*" | ls.getSymCour() == "/") {
        operateur = ls.getSymCour();
        ls.suivant();
    }
    else {
        erreur("<opMult>");
    }
    return operateur;
}

Noeud* LecteurPhraseAvecArbre::expBool() {
// <expBool> ::= <expBoolEt> { ou <expBoolEt> }

	Noeud* fact = expBoolEt();
	while (ls.getSymCour()=="ou" ) {
		Symbole operateur = opBool();
		Noeud* factDroit = expBoolEt();
                fact = new NoeudOperateurBinaire(operateur,fact,factDroit);
	}
        return fact;
}

Noeud* LecteurPhraseAvecArbre::expBoolEt() {
// <expBool> ::= <relation> { et <relation> }

	Noeud* fact = relation();
	while (ls.getSymCour()=="et") {
		Symbole operateur = opBool();
		Noeud* factDroit = relation();
                fact = new NoeudOperateurBinaire(operateur,fact,factDroit);
	}
        
        return fact;
}

Symbole LecteurPhraseAvecArbre::opBool() {
// <opBool> ::= et | ou
    Symbole operateur;
    if ( ls.getSymCour() == "et" || ls.getSymCour() == "ou") {
        operateur = ls.getSymCour();
        ls.suivant();
    }
    else {
        erreur("<opBool>");
    }
    return operateur;
}

Noeud* LecteurPhraseAvecArbre::relation() {
// <relation> ::= <expression> { <opRel> <expression> }

	Noeud* fact = expression();
	while (ls.getSymCour()=="==" || ls.getSymCour()=="!=" || ls.getSymCour()=="<" || ls.getSymCour()=="<=" || ls.getSymCour()==">" || ls.getSymCour()==">=") {
		Symbole operateur = opRel();
		Noeud* factDroit = expression();
                fact = new NoeudOperateurBinaire(operateur,fact,factDroit);
	}
        return fact;
}

Symbole LecteurPhraseAvecArbre::opRel() {
//<opRel> ::= == | != | < | <= | > | >=
    Symbole operateur;
        if (ls.getSymCour()=="==" || ls.getSymCour()=="!=" || ls.getSymCour()=="<" || ls.getSymCour()=="<=" || ls.getSymCour()==">" || ls.getSymCour()==">=") {
            operateur = ls.getSymCour();
            ls.suivant();
        }
        else {
            erreur("<opRel>");
        }
    return operateur;
}
Symbole LecteurPhraseAvecArbre::opUnaire() {
//<opUnaire> ::= - | non
    Symbole operateur;
        if (ls.getSymCour()=="-" || ls.getSymCour()=="non") {
            operateur = ls.getSymCour();
            ls.suivant();
        }
        else {
            erreur("<opUnaire>");
        }
    return operateur;
}

Noeud* LecteurPhraseAvecArbre::instSi(){
//<instSi> ::= si (<expBool>) <sqInst> {sinonsi (<expBool>) <seqInst>} [sinon <seqInst>] finsi
    sauterSymCour("si");
    sauterSymCour("(");
    Noeud*  exp = expBool();
    sauterSymCour(")");
    sauterSymCour("alors");
    Noeud* seq = seqInst();
    NoeudInstSi* instSi = new NoeudInstSi(exp, seq, NULL);
    NoeudInstSi* instSinon = instSi;
    while(ls.getSymCour() == "sinonsi") {

        sauterSymCour("sinonsi");
        sauterSymCour("(");
        exp = expBool();
        sauterSymCour(")");
        sauterSymCour("alors");
        seq = seqInst();
        instSinon = instSinon->setSinon(exp,seq);
    }
    if (ls.getSymCour() == "sinon") {
        sauterSymCour("sinon");
        seq = seqInst();
        instSinon = instSinon->setSinon(ts.chercheAjoute(Symbole("1")),seq);
    }
    sauterSymCour("finsi");

    return instSi;
}


Noeud* LecteurPhraseAvecArbre::instSwitch(){
//<instSwitch> ::= switch (<expression>) {case (<expression>): <seqInst>} [default : <seqInst>]
    sauterSymCour("switch");
    sauterSymCour("(");
    Noeud*  var = expression();
    sauterSymCour(")");
    sauterSymCour("case");
    Noeud* var2 = expression();
    Noeud* exp;                                            
    exp = new NoeudOperateurBinaire(Symbole("=="),var,var2);
    sauterSymCour(":");
    Noeud* seq = seqInst();
    NoeudInstSi* instSi = new NoeudInstSi(exp, seq, NULL);
    NoeudInstSi* instSinon = instSi;
    while(ls.getSymCour() == "case") {
        sauterSymCour("case");
        Noeud* var2 = expression();
        exp = new NoeudOperateurBinaire(Symbole("=="),var,var2);
        sauterSymCour(":");
        seq = seqInst();

        instSinon = instSinon->setSinon(exp,seq);
    }
    if (ls.getSymCour() == "default") {
        sauterSymCour("default");
        sauterSymCour(":");
        seq = seqInst();
        instSinon = instSinon->setSinon(ts.chercheAjoute(Symbole("1")),seq);
    }
    sauterSymCour("finswitch");
    return instSi;
}

Noeud* LecteurPhraseAvecArbre::instTq(){
//<instTq> ::= tantque (<expBool>) <seqInst> fintantque
    Noeud* condition;
    Noeud* seqI;
    Symbole tag = ls.getSymCour();
    sauterSymCour("tantque");
    sauterSymCour("(");
    condition = expBool();
    sauterSymCour(")");
    seqI = seqInst();
    sauterSymCour("fintantque");

    return new NoeudInstLoop(tag, condition, seqI);
}

Noeud* LecteurPhraseAvecArbre::instRepeter(){
//<instRepeter> ::= repeter <seqInst> jusqua (<expBool>)
    Noeud* condition;
    Noeud* seqI;
    Symbole tag = ls.getSymCour();
    sauterSymCour("repeter");
    seqI = seqInst();
    sauterSymCour("jusqua");
    sauterSymCour("(");
    condition = expBool();
    sauterSymCour(")");

    return new NoeudInstLoop(tag, condition, seqI);
}

Noeud* LecteurPhraseAvecArbre::instPour(){
//<instPour> ::= pour (<affectation> ; <expBool> ; <affectation>) <seqInst> finpour
    Noeud* condition;
    Noeud* seqI;
    Noeud* affect;
    Noeud* increment;
    Symbole tag = ls.getSymCour();

    sauterSymCour("pour");
    sauterSymCour("(");
    affect = affectation();
    sauterSymCour(";");
    condition = expBool();
    sauterSymCour(";");
    increment = affectation();
    sauterSymCour(")");
    seqI = seqInst();
    sauterSymCour("finpour");

    return new NoeudInstPour(tag, condition, seqI, affect, increment);
}

Noeud* LecteurPhraseAvecArbre::instLire(){
//<instLire> ::= lire (<variable>)
    sauterSymCour("lire");
    sauterSymCour("(");
    testerSymCour("<VARIABLE>");
    SymboleValue* var = ts.chercheAjoute(ls.getSymCour());
    ls.suivant();
    sauterSymCour(")");

    return new NoeudInstLire(var);
}

Noeud* LecteurPhraseAvecArbre::instEcrire(){
//<instEcrire> ::= ecrire (<expresison> | <chaine>)
    Noeud* varOut;
    sauterSymCour("ecrire");
    sauterSymCour("(");
    if (ls.getSymCour()=="<CHAINE>" || ls.getSymCour() =="<VARIABLE>") {
        varOut = ts.chercheAjoute(ls.getSymCour());
        ls.suivant();
    }
    else if( ls.getSymCour() == "<ENTIER>") {
        varOut = expression(); //fait deja un ls.suivant
    }
    sauterSymCour(")");

    return new NoeudInstEcrire(varOut);
}
