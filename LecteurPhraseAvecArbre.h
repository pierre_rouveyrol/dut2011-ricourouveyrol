#ifndef LECTEURPHRASEAVECARBRE_H_
#define LECTEURPHRASEAVECARBRE_H_

#include "Symbole.h"
#include "LecteurSymbole.h"
#include "TableSymboles.h"
#include "Arbre.h"
#include <string>

using namespace std;

/*! \file LecteurPhraseAvecArbre.h
    \brief La spécification de la classe LecteurPhraseAvecArbre
    Décrit la liste des méthodes et attributs associés à la classe LecteurPhraseAvecArbre
    \author Rouveyrol Pierre et Ricou Corentin
    \date 06.12.2011
*/
/*! \class LecteurPhraseAvecArbre
    \brief La classe représentant le Lecteur de phrase avec arbre.
    Cette classe permet de représenter sous la forme d'un arbre le fichier texte en parametre.
    Les méthodes suivantes sont associées à un objet de type LecteurPhraseAvecArbre :
    \li Noeud* programme();
    \li Noeud* seqInst();
    \li Noeud* inst();
    \li Noeud* affectation();
    \li Noeud* expression();
    \li Noeud* facteur();
    \li Noeud* terme();
    \li Noeud* expBool();
    \li Noeud* expBoolEt();
    \li Noeud* relation();
    \li void instSi();
    \li void instSwitch();
    \li void instTq();
    \li void instRepeter();
    \li void instPour();
    \li void instLire();
    \li void instEcrire();
    \li Symbole opAdd();
    \li Symbole opMult();
    \li Symbole opBool();
    \li Symbole opRel();
    \li Symbole opUnaire();
    \li void testerSymCour (string ch);
    \li void sauterSymCour (string ch);
    \li void erreur (string mess);
*/


class LecteurPhraseAvecArbre
{
public:
	LecteurPhraseAvecArbre(string nomFich);	 // Construit un lecteur de phrase pour interpreter
	                                         //  le programme dans le fichier nomFich

	void analyse();  // Si le contenu du fichier est conforme à la grammaire,
	                 //   cette méthode se termine normalement et affiche un message "Syntaxe correcte".
                         //   la table des symboles (ts) et l'arbre abstrait (arbre) auront été construits
	                 // Sinon, le programme sera interrompu (exit).

	inline TableSymboles getTs ()    { return ts;    } // accesseur	
	inline Noeud*        getArbre () { return arbre; } // accesseur

private:
    
    
    LecteurSymbole ls;
    /*! \var LecteurSymbole ls
        \brief le lecteur de symboles utilisé pour analyser le fichier
    */
    
    TableSymboles  ts;
    /*! \var TableSymboles  ts
        \brief la table des symboles valués
    */
    
    Noeud* arbre; 
    /*! \var Noeud* arbre;
        \brief l'arbre abstrait
    */
    // implémentation de la grammaire
    
    Noeud* programme();
    /*! \fn Noeud* programme()
        \brief <programme> ::= debut <seqInst> fin <EOF>
        \return un pointeur sur un noeud
    */
    
    Noeud* seqInst();
    /*! \fn Noeud* seqInst()
        \brief <seq_ins> ::= <inst> ; { <inst> ; }
        \return un pointeur sur un noeud
    */
    
    Noeud* inst();
    /*! \fn Noeud* inst()
        \brief <inst> ::= <affectation>
        \return un pointeur sur un noeud
    */
    Noeud* affectation();
    /*! \fn Noeud* affectation()
        \brief <affectation> ::= <variable> = <expression>
        \return un pointeur sur un noeud
    */
    Noeud* expression();
    /*! \fn Noeud* expression()
        \brief <expression> ::= <terme> { <opAdd> <terme> }
        \return un pointeur sur un noeud
    */
    Noeud* facteur();
    /*! \fn Noeud* facteur()
        \brief <facteur> ::= <entier>  |  <variable>  |  <opUnaire> (<expBool>) |  ( <expBool> )
        \return un pointeur sur un noeud
    */
    Noeud* terme();
    /*! \fn Noeud* terme()
        \brief <terme> ::= <facteur> { <opMult> <facteur> }
        \return un pointeur sur un noeud
    */
    Noeud* expBool();
    /*! \fn Noeud* expBool()
        \brief <expBool> ::= <relation> { <opBoolEt> <relation> }
        \return un pointeur sur un noeud
    */
    Noeud* expBoolEt();
    /*! \fn Noeud* expBoolEt()
        \brief <expBool> ::= <relation> { <opBool> <relation> }
        \return un pointeur sur un noeud
    */
    Noeud* relation();
    /*! \fn Noeud* relation()
        \brief <relation> ::= <expression> { <opRel> <expression> }
        \return un pointeur sur un noeud
    */
    Noeud* instSi();
    /*! \fn Noeud* instSi()
        \brief si (<expBool>) <sqInst> {sinonsi (<expBool>) <seqInst>} [sinon <seqInst>] finsi
        \return un pointeur sur un noeud
    */
    Noeud* instSwitch();
    /*! \fn Noeud* instSwitch()
        \brief <instSwitch> ::= switch (<variable>) {case (<expression>): <seqInst>} [default : <seqInst>]
        \return un pointeur sur un noeud
    */
    Noeud* instTq();
    /*! \fn Noeud* instTq()
        \brief tantque (<expBool>) <seqInst> fintantque
        \return un pointeur sur un noeud
    */
    Noeud* instRepeter();
    /*! \fn Noeud* instRepeter()
        \brief repeter <seqInst> jusqua (<expBool>)
        \return un pointeur sur un noeud
    */
    Noeud* instPour();
    /*! \fn Noeud* instPour()
        \brief <instPour> ::= pour (<affectation> & <expBool> & <affectation>) <seqInst> finpour
        \return un pointeur sur un noeud
    */
    Noeud* instLire();
    /*! \fn Noeud* instLire()
        \brief <instLire> ::= lire (<variable>)
        \return un pointeur sur un noeud
    */
    Noeud* instEcrire(); 
    /*! \fn Noeud* instEcrire()
        \brief <instEcrire> ::= ecrire (<relation> | <chaine>)
        \return un pointeur sur un noeud
    */
    Symbole opAdd();
    /*! \fn Symbole opAdd()
        \brief <opAdd> ::= - | +
        \return un Symbole
    */
    Symbole opMult();
     /*! \fn Symbole opMult()
        \brief <opMult> ::= * | /
        \return un Symbole
    */
    Symbole opBool();
    /*! \fn Symbole opBool()
        \brief <opBool> ::= et | ou
        \return un Symbole
    */
    Symbole opRel();
    /*! \fn Symbole opRel()
        \brief <opRel> ::= == | != | > | < | <= | >=
        \return un Symbole
    */
    Symbole opUnaire();    //<opUnaire> ::=  - | non
    /*! \fn Symbole opUnaire()
        \brief <opUnaire> ::=  - | non
        \return un Symbole
    */
    
    
    // outils pour se simplifier l'analyse syntaxique
    void testerSymCour (string ch); 
    /*! \fn testerSymCour (string ch)
     \brief si symbole courant != ch, erreur : on arrete le programme, sinon rien
     \param ch - une chaine qui représente le symbole courant
    */

    void sauterSymCour (string ch);
    /*! \fn sauterSymCour (string ch)
     \brief si symbole courant == ch, on passe au symbole suivant, sinon erreur : on arrete
     \param ch - une chaine qui représente le symbole courant
    */
    void erreur (string mess);      // affiche les message d'erreur mess et arrete le programme
    /*! \fn erreur (string mess)
     \brief affiche les message d'erreur mess et arrete le programme
     \param mess - le message d'erreur
    */
};

#endif /* LECTEURPHRASEAVECARBRE_H_ */
