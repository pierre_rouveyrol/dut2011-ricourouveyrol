#include <stdlib.h>
#include "Arbre.h"
#include "Symbole.h"
#include "SymboleValue.h"

////////////////////////////////////////////////////////////////////////////////
// NoeudSeqInst
////////////////////////////////////////////////////////////////////////////////

NoeudSeqInst::NoeudSeqInst() : tabInst() {
}

////////////////////////////////////////////////////////////////////////////////
Valeur* NoeudSeqInst::getValeur() {
  Valeur* valeur = NULL;
  for (unsigned int i=0; i<tabInst.size(); i++)
    valeur = tabInst[i]->getValeur();  // on evalue chaque instruction de la séquence
  return valeur; // par convention, resultat = valeur de la derniere instruction
}

////////////////////////////////////////////////////////////////////////////////
void NoeudSeqInst::afficher(unsigned short indentation) {
  Noeud::afficher(indentation);
  cout << "Noeud - Sequence de " << tabInst.size() << " instruction(s)" << endl;
  for (unsigned int i=0; i<tabInst.size(); i++)
    tabInst[i]->afficher(indentation+1); // on affiche les fils en augmentant l'indentation
}

string NoeudSeqInst::transCodage() {
    string code;
    for(unsigned int i=0; i< tabInst.size(); i++) {
        code = code + tabInst[i]->transCodage();
    }
    return code;
}

////////////////////////////////////////////////////////////////////////////////
void NoeudSeqInst::ajouteInstruction(Noeud* instruction) {
  tabInst.push_back(instruction);
}

////////////////////////////////////////////////////////////////////////////////
// NoeudAffectation
////////////////////////////////////////////////////////////////////////////////

NoeudAffectation::NoeudAffectation(Noeud* variable, Noeud* expression) {
  this->variable=variable;
  this->expression=expression;
}

////////////////////////////////////////////////////////////////////////////////
Valeur* NoeudAffectation::getValeur() {
  Valeur* valeur = expression->getValeur(); // on évalue l'expression
  
  if (((SymboleValue*)variable)->estDefini()){
      if(typeid(*(variable->getValeur())) != typeid(*(expression->getValeur()))) {
          cerr << "Erreur : Les variables doivent etre du meme type." <<endl << typeid(*(variable->getValeur())).name() << " != " << typeid(*(expression->getValeur())).name() << endl;
          exit(1);
      }
  }
  ((SymboleValue*)variable)->setValeur(valeur); // on affecte la variable
  return valeur; // par convention, une affectation a pour valeur la valeur affectée
}

////////////////////////////////////////////////////////////////////////////////
void NoeudAffectation::afficher(unsigned short indentation) {
  Noeud::afficher(indentation);
  cout << "Noeud - Affectation" << endl;
  variable->afficher(indentation+1);   // on affiche variable et expression
  expression->afficher(indentation+1); // en augmentant l'indentation
}

string NoeudAffectation::transCodage() {
    string code;
    code = ((SymboleValue*)variable)->getChaine() + "=" + expression->transCodage() + ";" ;

    return code;

}
////////////////////////////////////////////////////////////////////////////////
// NoeudOperateurBinaire
////////////////////////////////////////////////////////////////////////////////

NoeudOperateurBinaire::NoeudOperateurBinaire ( Symbole operateur,
                                               Noeud* operandeGauche,
                                               Noeud* operandeDroit) {
  this->operateur=operateur;
  this->operandeGauche=operandeGauche;
  this->operandeDroit=operandeDroit;
}

////////////////////////////////////////////////////////////////////////////////
Valeur* NoeudOperateurBinaire::getValeur() {
    Valeur* og=operandeGauche->getValeur();
    Valeur* od=operandeDroit->getValeur();

    if( typeid(*od) == typeid(*og) ) {
        if(typeid(*od) == typeid(ValeurChaine)) {
            ValeurChaine* vcg = dynamic_cast<ValeurChaine*> (og);
            ValeurChaine* vcd = dynamic_cast<ValeurChaine*> (od);
            ValeurChaine* valeur= new ValeurChaine();
            ValeurEntiere*valeurC = new ValeurEntiere();

            if (this->operateur=="+")  {valeur->setValeur(vcg->getValeur()+vcd->getValeur()); return valeur;}
            else if (this->operateur=="==") {valeurC->setValeur(vcg->getValeur()==vcd->getValeur()); return valeurC;}


        }
        else if(typeid(*od) == typeid(ValeurEntiere) ) {

                ValeurEntiere* vcg = dynamic_cast<ValeurEntiere*> (og);
                ValeurEntiere* vcd = dynamic_cast<ValeurEntiere*> (od);
                ValeurEntiere* valeur = new ValeurEntiere();

                ///////////////////////////////////////////////////////:
                if(this->operateur=="+") valeur->setValeur(vcg->getValeur()+vcd->getValeur());
                else if (this->operateur=="-") valeur->setValeur(vcg->getValeur()-vcd->getValeur());
                else if (this->operateur=="*") valeur->setValeur(vcg->getValeur()*vcd->getValeur());
                else if (this->operateur=="^") valeur->setValeur(vcg->getValeur()^vcd->getValeur());
                else if (this->operateur=="==") valeur->setValeur(vcg->getValeur()==vcd->getValeur());
                else if (this->operateur=="!=") valeur->setValeur(vcg->getValeur()!=vcd->getValeur());
                else if (this->operateur=="<") valeur->setValeur(vcg->getValeur()<vcd->getValeur());
                else if (this->operateur==">") valeur->setValeur(vcg->getValeur()>vcd->getValeur());
                else if (this->operateur=="<=") valeur->setValeur(vcg->getValeur()<=vcd->getValeur());
                else if (this->operateur==">=") valeur->setValeur(vcg->getValeur()>=vcd->getValeur());
                else if (this->operateur=="ou") { valeur->setValeur(vcg->getValeur()||vcd->getValeur());}
                else if (this->operateur=="et") { valeur->setValeur(vcg->getValeur()&&vcd->getValeur());}
                else  // this->operateur=="/"
                if (vcd->getValeur()!=0)
                    valeur->setValeur(vcg->getValeur()/vcd->getValeur());
                else {
                    cout << "Erreur pendant l'interpretation : division par zero" << endl;
                    exit(0); // plus tard on levera une exception
                }
                return valeur;

            } else if(typeid(*od) == typeid(ValeurReelle)) {
                ValeurReelle* vcg = dynamic_cast<ValeurReelle*> (og);
                ValeurReelle* vcd = dynamic_cast<ValeurReelle*> (od);
                ValeurReelle* valeur = new ValeurReelle();
                ValeurEntiere* valeurE = new ValeurEntiere();

             if(this->operateur=="+") {valeur->setValeur(vcg->getValeur()+vcd->getValeur()); return valeur;}
            else if (this->operateur=="-") { valeur->setValeur(vcg->getValeur()-vcd->getValeur()); return valeur; }
            else if (this->operateur=="*") { valeur->setValeur(vcg->getValeur()*vcd->getValeur()); return valeur; }
            else if (this->operateur=="==") { valeurE->setValeur(vcg->getValeur()==vcd->getValeur()); return valeurE;}
            else if (this->operateur=="!=") { valeurE->setValeur(vcg->getValeur()!=vcd->getValeur()); return valeurE;}
            else if (this->operateur=="<") { valeurE->setValeur(vcg->getValeur()<vcd->getValeur()); return valeurE;}
            else if (this->operateur==">") { valeurE->setValeur(vcg->getValeur()>vcd->getValeur()); return valeurE;}
            else if (this->operateur=="<=") { valeurE->setValeur(vcg->getValeur()<=vcd->getValeur()); return valeurE;}
            else if (this->operateur==">=") { valeurE->setValeur(vcg->getValeur()>=vcd->getValeur()); return valeurE;}
            else if (this->operateur=="ou") { valeurE->setValeur(vcg->getValeur()||vcd->getValeur()); return valeurE;}
            else if (this->operateur=="et") { valeurE->setValeur(vcg->getValeur()&&vcd->getValeur()); return valeurE;}
            else  // this->operateur=="/"
            if (vcd->getValeur()!=0) {
                valeur->setValeur(vcg->getValeur()/vcd->getValeur());
                return valeur;
             }else {
                cout << "Erreur pendant l'interpretation : division par zero" << endl;
                exit(0); // plus tard on levera une exception
            }
            }else {
                cout << "type des operandes inconnu" << endl;
                exit(0);
            }
    } else {
        cout << "Erreur : le type des deux operandes est different"<< endl;
        exit(0);
    }
}

////////////////////////////////////////////////////////////////////////////////
void NoeudOperateurBinaire::afficher(unsigned short indentation) {
  Noeud::afficher(indentation);
  cout << "Noeud - Operateur Binaire \"" << this->operateur.getChaine() << "\" applique a : " << endl;
  operandeGauche->afficher(indentation+1);  // on affiche fils gauche et fils droit
  operandeDroit->afficher(indentation+1);   // en augmentant l'indentation
}

string NoeudOperateurBinaire::transCodage() {

    string code;
    string op=operateur.getChaine();
    if (operateur.getChaine()=="et") op=" && ";
    else if (operateur.getChaine()=="ou") op=" || ";
    code = "(" + operandeGauche->transCodage() + op + operandeDroit->transCodage() + ")";
    return code;
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstLoop
////////////////////////////////////////////////////////////////////////////////


NoeudInstLoop::NoeudInstLoop (Symbole tag,
                              Noeud* condition,
                              Noeud* seqInst) {
  this->tag=tag;
  this->condition=condition;
  this->seqInst=seqInst;
}

Valeur* NoeudInstLoop::getValeur() {

    Valeur* val;
    ValeurEntiere* vale;
    if (this->tag.getChaine() == "tantque") {
        vale = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
        while(vale->getValeur()) {
           val = this->seqInst->getValeur();
           vale = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
        }
    }
    else if (this->tag.getChaine() == "repeter") {
        do{
            val = this->seqInst->getValeur();
            vale = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
        } while (!(vale->getValeur()));
    }
return val;
}

void NoeudInstLoop::afficher(unsigned short indentation) {
  Noeud::afficher(indentation);
  cout << "Noeud - structure iterative \"" << this->tag.getChaine() << "\" applique a : " << endl;
  condition->afficher(indentation+1);  // on affiche fils gauche et fils droit
  seqInst->afficher(indentation+1);   // en augmentant l'indentation
}

string NoeudInstLoop::transCodage() {

    string code="";
    if (this->tag.getChaine() == "tantque") {
        code = "\nwhile(" + condition->transCodage() + ") {\n\t" + seqInst->transCodage() + "\n}\n" ;
    } else if (this->tag.getChaine() == "repeter") {
        code = "do {\n\t" + seqInst->transCodage() + "\n} while( !(" + condition->transCodage() + "));\n";
    }
    return code;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstPour
//////////////////////////////////////////////////////////////////////new NoeudOperateurBinaire(operateur,fact,factDroit);//////////


NoeudInstPour::NoeudInstPour (Symbole tag,
                              Noeud* condition,
                              Noeud* seqInst,
                              Noeud* affectation,
                              Noeud* incrementation) : NoeudInstLoop (tag, condition, seqInst)
{
    this->affectation=affectation;
    this->incrementation=incrementation;
}

Valeur* NoeudInstPour::getValeur() {
    Valeur* val;
    Valeur* test = this->affectation->getValeur();

    if(typeid(*test) == typeid(ValeurEntiere)) {

        ValeurEntiere* affect = dynamic_cast<ValeurEntiere*> (this->affectation->getValeur());
        ValeurEntiere* cond = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
        ValeurEntiere* incre = new ValeurEntiere();

            for( affect->getValeur();cond->getValeur();incre->getValeur()) {
               val = this->seqInst->getValeur();
               incre = dynamic_cast<ValeurEntiere*> (this->incrementation->getValeur());
               cond = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
            }

        return val;
    } else if(typeid(*test) == typeid(ValeurReelle)) {

        ValeurReelle* affect = dynamic_cast<ValeurReelle*> (this->affectation->getValeur());
        ValeurEntiere* cond = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
        ValeurReelle* incre = new ValeurReelle();

            for( affect->getValeur();cond->getValeur();incre->getValeur()) {
               val = this->seqInst->getValeur();
               incre = dynamic_cast<ValeurReelle*> (this->incrementation->getValeur());
               cond = dynamic_cast<ValeurEntiere*> (this->condition->getValeur());
            }

        return val;
    }
    else {
        cout << "Impossible d'utiliser une chaine pour la boucle FOR" << endl;
        exit(0);
    }
}

void NoeudInstPour::afficher(unsigned short indentation){
    NoeudInstLoop::afficher(indentation);
    affectation->afficher(indentation+1);
    incrementation->afficher(indentation+1);
}

string NoeudInstPour::transCodage() {

    string code;
    string tmp;
    size_t size;
    if (this->tag.getChaine() == "pour") {
        tmp = incrementation->transCodage();
        size = (tmp.size())-1;
        tmp.resize(size);
        code = "\nfor(" + affectation->transCodage() + condition->transCodage() + ";" + tmp + "){\n" + seqInst->transCodage() + "\n}\n";
    }
    return code;
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstSi
////////////////////////////////////////////////////////////////////////////////

NoeudInstSi::NoeudInstSi(Noeud* expBool,Noeud* seqInst, NoeudInstSi* Sinon ) {
    this->expBool = expBool;
    this->seqInst = seqInst;
    this->sinon = Sinon;
}

Valeur* NoeudInstSi::getValeur() {
    ValeurEntiere* cond = dynamic_cast<ValeurEntiere*> (expBool->getValeur());
    if(cond->getValeur())
        return seqInst->getValeur();
     else if(sinon)
        return sinon->getValeur();
}

void NoeudInstSi::afficher(unsigned short indentation){
    Noeud::afficher(indentation);
     cout << "Noeud - structure controle"<< endl;
    expBool->afficher(indentation+1);
    seqInst->afficher(indentation+1);
    if(sinon)
        sinon->afficher(indentation+1);
}

string NoeudInstSi::transCodage() {

    string code;
    code = "if(" + expBool->transCodage() + ") {\n\t" + seqInst->transCodage() + "\n}\n";
    if (this->sinon) {
        code = code + "else " + this->sinon->transCodage();
    }
    return code;
}
////var->transCodage()////////////////////////////////////////////////////////////////////////////
// NoeudInstLire
////////////////////////////////////////////////////////////////////////////////

NoeudInstLire::NoeudInstLire(Noeud* var) {
    this->var = var;
}

Valeur* NoeudInstLire::getValeur() {
    //TODO faire un vod* et utiliser un typeid dessus
    /*int valeur;
    cin >> valeur;
    ((SymboleValue*)var)->setValeur(valeur);*/
    //return var->getValeur();

if(typeid(*(var->getValeur())) == typeid(ValeurEntiere))  {
     ValeurEntiere* valeur = new ValeurEntiere();
     int val;
     cin >> val;
     valeur->setValeur(val);
     ((SymboleValue*)var)->setValeur(valeur);
     return valeur;
} else if(typeid(*(var->getValeur())) == typeid(ValeurReelle)) {
     ValeurReelle* valeur = new ValeurReelle();
     float val;
     cin >> val;
     valeur->setValeur(val);
     ((SymboleValue*)var)->setValeur(valeur);
     return valeur;
} else if (typeid(*(var->getValeur())) == typeid(ValeurChaine)) {
     ValeurChaine* valeur = new ValeurChaine();
     string val;
     cin >> val;
     val.insert(0,"\"");
     val.append("\"");
     valeur->setValeur(val);
     ((SymboleValue*)var)->setValeur(valeur);
     return valeur;
     } else if (typeid(*(var->getValeur())) == typeid(ValeurChar)) {
     ValeurChar* valeur = new ValeurChar();
     char val;
     cin >> val;
     valeur->setValeur(val);
     ((SymboleValue*)var)->setValeur(valeur);
     return valeur;
}else {
    cout << "mauvais type en lecture" << endl;
    exit(0);
}

}

void NoeudInstLire::afficher(unsigned short indentation) {
    Noeud::afficher(indentation);
    cout << "Noeud - intruction Lire"<< endl;
    var->afficher(indentation+1);
}

string NoeudInstLire::transCodage() {

     string code;
     code = "cin >> " + var->transCodage() + ";\n";
     
     return code;
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstLire
////////////////////////////////////////////////////////////////////////////////

NoeudInstEcrire::NoeudInstEcrire(Noeud* var) {
    this->var = var;
}

Valeur* NoeudInstEcrire::getValeur() {
    Valeur* val = var->getValeur();
    if(typeid(*val)== typeid(ValeurEntiere)) {
        ValeurEntiere* valeur = dynamic_cast<ValeurEntiere*> (val);
        cout << valeur->getValeur() << endl;
        return valeur;

    }else if(typeid(*val)== typeid(ValeurReelle)) {
        ValeurReelle* valeur = dynamic_cast<ValeurReelle*> (val);
        cout << valeur->getValeur() << endl;
        return valeur;

    }else if(typeid(*val)== typeid(ValeurChaine)) {
        ValeurChaine* valeur = dynamic_cast<ValeurChaine*> (val);
        string var = valeur->getValeur();
        var.erase(0,1);
        var.erase(var.end()-1);
        cout << var << endl;
        return valeur;
    }else if(typeid(*val)== typeid(ValeurChar)) {
        ValeurChar* valeur = dynamic_cast<ValeurChar*> (val);
        cout << valeur->getValeur() << endl;
        return valeur;
    }
    else {
        cout << "mauvais type" << endl;
        exit(0);
    }
}

void NoeudInstEcrire::afficher(unsigned short indentation) {
    Noeud::afficher(indentation);
    cout << "Noeud - intruction Lire"<< endl;
    var->afficher(indentation+1);
}

string NoeudInstEcrire::transCodage() {

     string code;
     code = "cout << " + var->transCodage() + " << endl;\n";
     return code;
}
