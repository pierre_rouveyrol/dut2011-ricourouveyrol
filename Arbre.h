// Contient toutes les spécifications de classes nécessaires
// pour représenter l'arbre abstrait
#ifndef ARBRE_H_
#define ARBRE_H_

#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

#include "Symbole.h"
#include "valeur.h"

////////////////////////////////////////////////////////////////////////////////
class Noeud {
// classe abstraite dont dériveront toutes les classes servant à représenter l'arbre abstrait
// Remarque : la classe ne contient aucun constructeur
  public:
    virtual Valeur*  getValeur() {} ; // méthode pure (non implémentée) qui rend la classe abstraite
    virtual void afficher(unsigned short indentation=0) { cout << setw(4*indentation) << " "; }
      // pour afficher un noeud avec un décalage (indentation) proportionnel à son niveau dans l'arbre
    virtual string transCodage() {};

    virtual ~Noeud() {} // présence d'un destructeur virtuel conseillée dans les classes abstraites
};

////////////////////////////////////////////////////////////////////////////////
class NoeudSeqInst : public Noeud {
// classe pour représenter un noeud "sequence d'instruction"
// qui a autant de fils que d'instructions dans la séquence
  public:
     NoeudSeqInst();   // construit une séquence d'instruction vide
    ~NoeudSeqInst() {} // à cause du destructeur virtuel de la classe Noeud
    Valeur* getValeur(); // évalue chaque instruction de la séquence
    void afficher(unsigned short indentation=0); // affiche la séquence d'instructions
    void ajouteInstruction(Noeud* instruction);  // ajoute une instruction à la séquence
    string transCodage();
  private:
    vector<Noeud *> tabInst; // pour stocker les instructions de la séquence
};

////////////////////////////////////////////////////////////////////////////////
class NoeudAffectation : public Noeud {
// classe pour représenter un noeud "affectation"
// composé de 2 fils : la variable et l'expression qu'on lui affecte
  public:
     NoeudAffectation(Noeud* variable, Noeud* expression); // construit une affectation
    ~NoeudAffectation() {} // à cause du destructeur virtuel de la classe Noeud
    Valeur*  getValeur(); // évalue l'expression et affecte sa valeur à la variable
    void afficher(unsigned short indentation=0); // affiche l'affectation
    string transCodage();
  private:
    Noeud* variable;
    Noeud* expression;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudOperateurBinaire : public Noeud {
// classe pour représenter un noeud "opération arithmétique" composé d'un opérateur (+-*/)
// et de 2 fils : l'opérande gauche et l'opérande droit
  public:
    NoeudOperateurBinaire(Symbole operateur, Noeud* operandeGauche, Noeud* operandeDroit);
     // construit une opération binaire : operandeGauche operateur OperandeDroit
   ~NoeudOperateurBinaire() {} // à cause du destructeur virtuel de la classe Noeud
    Valeur*  getValeur(); // évalue l'operande gauche, l'operande droit et applique l'opérateur
    void afficher(unsigned short indentation=0); // affiche l'opération
    string transCodage();
  private:
    Symbole operateur;
    Noeud*  operandeGauche;
    Noeud*  operandeDroit;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstLoop : public Noeud{
public:
    NoeudInstLoop(Symbole tag, Noeud* condition, Noeud* seqInst);
    ~NoeudInstLoop(){};
    Valeur*  getValeur(); // évalue l'operande gauche, l'operande droit et applique l'opérateur
    void afficher(unsigned short indentation=0); // affiche l'opération
    string transCodage();
protected:
    Noeud* condition;
    Noeud* seqInst;
    Symbole tag;
};

////////////////////////////////////////////////////////////////////////////////
class NoeudInstPour : public NoeudInstLoop {
public:
    NoeudInstPour(Symbole tag,
                  Noeud* condition,
                  Noeud* seqInst,
                  Noeud* affectation,
                  Noeud* incrementation);
    ~NoeudInstPour(){};
    Valeur*  getValeur();
    void afficher(unsigned short indentation=0);
    string transCodage();
private:
    Noeud* affectation;
    Noeud* incrementation;
};

class NoeudInstSi : public Noeud {
public:
    NoeudInstSi(Noeud* expBool,Noeud* seqInst, NoeudInstSi* Sinon );
    ~NoeudInstSi(){};
    Valeur*  getValeur();
    void afficher(unsigned short indentation=0);
    inline NoeudInstSi* setSinon(Noeud* expBool,Noeud* seqInst) { this->sinon = new NoeudInstSi(expBool, seqInst,NULL); return this->sinon; };
    string transCodage();
private:
    Noeud* expBool;
    Noeud* seqInst;
    NoeudInstSi* sinon;
};

class NoeudInstLire : public Noeud {
public:
    NoeudInstLire(Noeud* var);
    ~NoeudInstLire(){};
    Valeur*  getValeur();
    void afficher(unsigned short indentation = 0);
    string transCodage();

private:
    Noeud* var;
};

class NoeudInstEcrire : public Noeud {
public:
    NoeudInstEcrire(Noeud* var);
    ~NoeudInstEcrire() {};
    Valeur*  getValeur();
    void afficher(unsigned short indentation = 0);
    string transCodage();

private:
    Noeud* var;
};

#endif /* ARBRE_H_ */
