#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/SymboleValue.o \
	${OBJECTDIR}/LecteurPhraseAvecTable.o \
	${OBJECTDIR}/LecteurCaractere.o \
	${OBJECTDIR}/Arbre.o \
	${OBJECTDIR}/Symbole.o \
	${OBJECTDIR}/LecteurPhraseSimple.o \
	${OBJECTDIR}/TestLecteurPhraseAvecArbre.o \
	${OBJECTDIR}/LecteurPhraseAvecArbre.o \
	${OBJECTDIR}/LecteurSymbole.o \
	${OBJECTDIR}/TableSymboles.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/inf1-2011-ricourouveyrol

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/inf1-2011-ricourouveyrol: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/inf1-2011-ricourouveyrol ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/SymboleValue.o: SymboleValue.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/SymboleValue.o SymboleValue.cpp

${OBJECTDIR}/LecteurPhraseAvecTable.o: LecteurPhraseAvecTable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/LecteurPhraseAvecTable.o LecteurPhraseAvecTable.cpp

${OBJECTDIR}/LecteurCaractere.o: LecteurCaractere.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/LecteurCaractere.o LecteurCaractere.cpp

${OBJECTDIR}/Arbre.o: Arbre.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Arbre.o Arbre.cpp

${OBJECTDIR}/Symbole.o: Symbole.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Symbole.o Symbole.cpp

${OBJECTDIR}/LecteurPhraseSimple.o: LecteurPhraseSimple.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/LecteurPhraseSimple.o LecteurPhraseSimple.cpp

${OBJECTDIR}/TestLecteurPhraseAvecArbre.o: TestLecteurPhraseAvecArbre.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/TestLecteurPhraseAvecArbre.o TestLecteurPhraseAvecArbre.cpp

${OBJECTDIR}/LecteurPhraseAvecArbre.o: LecteurPhraseAvecArbre.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/LecteurPhraseAvecArbre.o LecteurPhraseAvecArbre.cpp

${OBJECTDIR}/LecteurSymbole.o: LecteurSymbole.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/LecteurSymbole.o LecteurSymbole.cpp

${OBJECTDIR}/TableSymboles.o: TableSymboles.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/TableSymboles.o TableSymboles.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/inf1-2011-ricourouveyrol

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
