#include "SymboleValue.h"
#include <stdlib.h>



////////////////////////////////////////////////////////////////////////////////
SymboleValue::SymboleValue(Symbole s) :
	Symbole(s.getChaine()) {
	if (s == "<ENTIER>") {
	    ValeurEntiere* ve = new ValeurEntiere(atoi(s.getChaine().c_str()));
		this->valeur = ve;
		defini = true;
    }else if(s=="<REEL>"){
        ValeurReelle* vr = new ValeurReelle(atof(s.getChaine().c_str()));
        this->valeur = vr;
        defini = true;
    } else if(s=="<CHAINE>"){
        ValeurChaine* vc = new ValeurChaine(s.getChaine());
        this->valeur = vc ;
        defini = true;
    } else if(s=="<CARACTERE>"){
        ValeurChar* vch = new ValeurChar(s.getChaine()[1]);
        cout << vch->getValeur() << endl ;
        this->valeur = vch ;
        defini = true;
    }else {
		valeur = NULL;
		defini = false;
	}
}

////////////////////////////////////////////////////////////////////////////////
void SymboleValue::afficher(unsigned short indentation) {
	Noeud::afficher(indentation);
	cout << "Feuille - Symbole value : " << getChaine() << endl; // on affiche la chaine du symbole
}

////////////////////////////////////////////////////////////////////////////////
string SymboleValue::transCodage(){
    return getChaine();
}

////////////////////////////////////////////////////////////////////////////////
// Attention : cette fonction (operator) n'est pas membre de la classe SymboleValue
ostream & operator <<(ostream & cout, SymboleValue symb) {
	cout << (Symbole) symb << "\t\t - Valeur=";
	if (symb.defini) {
        if(typeid(*symb.valeur)==typeid(ValeurEntiere))
        {
            ValeurEntiere* ve = dynamic_cast<ValeurEntiere*> (symb.valeur);
            cout << ve->getValeur() << " ";
        }
        else if(typeid(*symb.valeur)==typeid(ValeurChaine))
        {
            ValeurChaine* ve = dynamic_cast<ValeurChaine*> (symb.valeur);
            cout << ve->getValeur() << " ";
        }
        else if(typeid(*symb.valeur)==typeid(ValeurReelle))
        {
            ValeurReelle* ve = dynamic_cast<ValeurReelle*> (symb.valeur);
            cout << ve->getValeur() << " ";
        }
        else if(typeid(*symb.valeur)==typeid(ValeurChar))
        {
            ValeurChar* ve = dynamic_cast<ValeurChar*> (symb.valeur);
            cout << ve->getValeur() << " ";
        }
	}
	else
		cout << "indefinie ";
	return cout;
}
//////////////////////////////////////////////////////////////////////////////////
