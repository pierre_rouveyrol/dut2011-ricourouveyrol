#ifndef EXCEPTION_H
#define	EXCEPTION_H

#include <exception>
using namespace std;

class ErreurType: public exception
{
public:
     ErreurType(string const& quoi="", int ligne=0, int colonne=0, string attendu="", string trouve="") throw()
         :quoi(quoi),ligne(ligne),colonne(colonne),attendu(attendu),trouve(trouve)
     {}
         
     virtual ~ErreurType() throw()
     {}
 
     virtual const char* what() const throw()
     {
         return quoi.c_str();
     }
     
     int getLigne() const throw()
     {
          return ligne;
     }
     
     int getColonne() const throw()
     {
          return colonne;
     }
     
     string getAttendu() const throw()
     {
          return attendu;
     }
          
     string getTrouve() const throw()
     {
          return trouve;
     }
    

 
private:
    string quoi;            // Description de l'erreur.
    unsigned int ligne;                // Ligne de l'erreur.
    unsigned int colonne;              // Colonne de l'erreur.
    string attendu;           // Type attendu.
    string trouve;            // Type trouvé.
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class ErreurFichier: public exception
{  
public:
     ErreurFichier(string const& quoi="", string recherche="") throw()
         :quoi(quoi),recherche(recherche)
     {}
         
     virtual ~ErreurFichier() throw()
     {}
 
     virtual const char* what() const throw()
     {
         return quoi.c_str();
     }
     
     string getRecherche() const throw()
     {
          return recherche;
     }

    

 
private:
    string quoi;            // Description de l'erreur.
    string recherche;       // Fichier recherché.
};




#endif	/* EXCEPTION_H */

