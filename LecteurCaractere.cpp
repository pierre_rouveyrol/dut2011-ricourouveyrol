#include <stdlib.h>
#include <iostream>
using namespace std;

#include "LecteurCaractere.h"
#include "Exceptions.h"

////////////////////////////////////////////////////////////////////////////////
LecteurCaractere::LecteurCaractere(string nomFich) : f(nomFich.data())
{
  ligne=1;
  colonne=0;
  try{
        if (f.fail())
        {
            //cout << "Fichier \"" << nomFich << "\" non trouve." << endl;
            // exit(0);
            throw ErreurFichier("Fichier non trouvé : ", nomFich);
        }
        else
            suivant();
  }
  catch(ErreurFichier const& e){
        cerr << e.what() << e.getRecherche() << endl;
        exit(1);
  }
}

////////////////////////////////////////////////////////////////////////////////
void LecteurCaractere::suivant()
{
  if (f.peek()==EOF)
    carCour=EOF;
  else
  {
    if (carCour=='\n')
    {
      colonne=0;
      ligne++;
    }
    f.get(carCour);
    colonne++;
  }
}
