#ifndef VALEUR_H
#define VALEUR_H

#include <typeinfo>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

class Valeur {
public:
    virtual ~Valeur() {};
};

class ValeurEntiere : public Valeur {
public:
    ValeurEntiere(int val=0) {
    this->val=val;
    }
    int getValeur() { return val; }
    void setValeur(int valeur) { this->val = valeur; }
private:
    int val;
};

class ValeurReelle : public Valeur {
public:
    ValeurReelle(float val=0.0) {this->val=val;}
    float getValeur() { return val; }
    void setValeur(float valeur) { this->val = valeur;}
private:
    float val;
};

class ValeurChaine : public Valeur {
public:
    ValeurChaine(string val="") {this->val=val;}
    string getValeur() { return val; }
    void setValeur(string valeur) { this->val = valeur;}
private:
    string val;
};

class ValeurChar : public Valeur {
public:
    ValeurChar(char val=' ') {this->val=val;}
    char getValeur() { return val; }
    void setValeur(char valeur) { this->val = valeur;}
private:
    char val;
};


#endif // VALEUR_H
